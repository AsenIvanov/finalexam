package pages;

import org.openqa.selenium.By;
import utils.Browser;

public class AccountLoginPage {
    private static final By LOC_MY_ACCOUNT_DROPDOWN = By.xpath("//a[@title='My Account']");
    private static final By LOC_LOGIN_BUTTON = By.xpath("//a[text()='Login']");
    private static final By LOC_EMAIL_ADDRESS = By.id("input-email");
    private static final By LOC_PASSWORD = By.id("input-password");
    private static final By LOC_ACCOUNT_LOGIN_BUTT = By.xpath("//input[@class='btn btn-primary']");


    public static void goTo() {
        Browser.driver.findElement(LOC_MY_ACCOUNT_DROPDOWN).click();
        Browser.driver.findElement(LOC_LOGIN_BUTTON).click();
    }

    public static void setCredentialsAndLogin(String email, String password) {
        Browser.driver.findElement(LOC_EMAIL_ADDRESS).sendKeys(email);
        Browser.driver.findElement(LOC_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOC_ACCOUNT_LOGIN_BUTT).click();
    }
}

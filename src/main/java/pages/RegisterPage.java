package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.Browser;

public class RegisterPage {

private static final By LOC_MY_ACCOUNT_DROPDOWN = By.xpath("//a[@title='My Account']");
private static final By LOC_REGISTER_BUTTON = By.xpath("//a[text()='Register']");
private static final By LOC_FIRST_NAME = By.id("input-firstname");
private static final By LOC_LAST_NAME = By.id("input-lastname");
private static final By LOC_EMAIL = By.id("input-email");
private static final By LOC_TELEPHONE = By.id("input-telephone");
private static final By LOC_PASSWORD = By.id("input-password");
private static final By LOC_PASSWORD_CONFIRM = By.id("input-confirm");
private static final By LOC_SUBSCRIBE_BUTT_YES = By.xpath("//label[@class='radio-inline']//input[@value='1']");
private static final By LOC_SUBSCRIBE_BUTT_NO = By.xpath("//label[@class='radio-inline']//input[@value='0']");
private static final By LOC_PRIVACY_CHECKBOX = By.xpath("//input[@type='checkbox']");
private static final By LOC_CONTINUE_BUTTON = By.xpath("//input[@class='btn btn-primary']");


    public static void goTo() {
        Browser.driver.findElement(LOC_MY_ACCOUNT_DROPDOWN).click();
        Browser.driver.findElement(LOC_REGISTER_BUTTON).click();
    }
    public static void setPersonalDetails(String firstName,String lastName,String email, String phoneNum,String password,String passwordConfirm) {
        Browser.driver.findElement(LOC_FIRST_NAME).sendKeys(firstName);
        Browser.driver.findElement(LOC_LAST_NAME).sendKeys(lastName);
        Browser.driver.findElement(LOC_EMAIL).sendKeys(email);
        Browser.driver.findElement(LOC_TELEPHONE).sendKeys(phoneNum);
        Browser.driver.findElement(LOC_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOC_PASSWORD_CONFIRM).sendKeys(passwordConfirm);
    }
    public static void selectSubscribeOption(String chosenOption) {
        switch (chosenOption) {
            case "yes":
                WebElement yesRadioButt = Browser.driver.findElement(LOC_SUBSCRIBE_BUTT_YES);
                if (!yesRadioButt.isSelected()) {
                    yesRadioButt.click();
                }
                break;

            case "no":
                WebElement noRadioButt = Browser.driver.findElement(LOC_SUBSCRIBE_BUTT_NO);
                if (!noRadioButt.isSelected()) {
                    noRadioButt.click();
                }
                break;
            default:
                throw new RuntimeException("Selected option " + chosenOption + " is not valid !");
        }
    }
    public static void privacyPolicyAgreement(){
        WebElement checkBoxButt = Browser.driver.findElement(LOC_PRIVACY_CHECKBOX);
        if (!checkBoxButt.isSelected()){
            checkBoxButt.click();
        }
    }
    public static void submitRegistrationDetails(){
        Browser.driver.findElement(LOC_CONTINUE_BUTTON).click();
    }

}

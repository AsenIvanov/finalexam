package pages;
import static org.testng.Assert.*;
import org.openqa.selenium.By;
import utils.Browser;

public class RegisterPageErrorMessages {
private static final By LOC_FIRST_NAME_ERROR = By.xpath("//div[text()='First Name must be between 1 and 32 characters!']");
private static final By LOC_LAST_NAME_ERROR = By.xpath("//div[text()='Last Name must be between 1 and 32 characters!']");
private static final By LOC_MAIL_ERROR = By.xpath("//div[text()='E-Mail Address does not appear to be valid!']");
private static final By LOC_PHONE_ERROR = By.xpath("//div[text()='Telephone must be between 3 and 32 characters!']");
private static final By LOC_PASSWORD_ERROR = By.xpath("//div[text()='Password must be between 4 and 20 characters!']");

    public static void verify() {
        assertEquals(Browser.driver.findElement(LOC_FIRST_NAME_ERROR).getText(),"First Name must be between 1 and 32 characters!");
        assertEquals(Browser.driver.findElement(LOC_LAST_NAME_ERROR).getText(),"Last Name must be between 1 and 32 characters!");
        assertEquals(Browser.driver.findElement(LOC_MAIL_ERROR).getText(),"E-Mail Address does not appear to be valid!");
        assertEquals(Browser.driver.findElement(LOC_PHONE_ERROR).getText(),"Telephone must be between 3 and 32 characters!");
        assertEquals(Browser.driver.findElement(LOC_PASSWORD_ERROR).getText(),"Password must be between 4 and 20 characters!");
    }
}

package utils;

import org.testng.Assert;

public class CommonVerification {

    public static void verifyTitle(String expectedTitle) {
        String actualTitle = Browser.driver.getTitle();
        Assert.assertEquals(actualTitle,expectedTitle);
    }
}

package registration.negative;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.RegisterPage;
import pages.RegisterPageErrorMessages;
import utils.Browser;

public class NegativeRegistrationTests {

    @BeforeMethod
    public static void setUp(){
        Browser.open();
        HomePage.goTo();
        RegisterPage.goTo();
    }

    @AfterMethod
    public static void tearDown(){
        Browser.close();
    }

    @Test
    public static void regWithoutDetailsTest(){
        RegisterPage.submitRegistrationDetails();
        RegisterPageErrorMessages.verify();
    }
}

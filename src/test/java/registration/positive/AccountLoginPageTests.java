package registration.positive;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AccountLoginPage;
import pages.HomePage;
import pages.RegisterPage;
import utils.Browser;
import utils.CommonVerification;

public class AccountLoginPageTests {
    @BeforeMethod
    public static void setUp(){
        Browser.open();
        HomePage.goTo();
        AccountLoginPage.goTo();
    }
    @AfterMethod
    public static void tearDown(){
        Browser.close();
    }
    @Test
    public static void loginTest(){
        CommonVerification.verifyTitle("Account Login");
        AccountLoginPage.setCredentialsAndLogin("aaaaa@abv.bg","1qaz");
        CommonVerification.verifyTitle("My Account");
    }

}

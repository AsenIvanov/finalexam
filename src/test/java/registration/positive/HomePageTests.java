package registration.positive;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.RegisterPage;
import utils.Browser;
import utils.CommonVerification;

public class HomePageTests {
    @BeforeMethod
    public static void setUp(){
        Browser.open();
    }

    @AfterMethod
    public static void tearDown(){
        Browser.close();
    }
    @Test
    public static void registrationTest(){
        HomePage.goTo();
        CommonVerification.verifyTitle("Pragmatic Test Store");
        RegisterPage.goTo();
        CommonVerification.verifyTitle("Register Account");
    }

}

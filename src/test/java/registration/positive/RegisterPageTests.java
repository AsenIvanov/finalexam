package registration.positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AccountLoginPage;
import pages.HomePage;
import pages.RegisterPage;
import utils.Browser;
import utils.CommonVerification;

public class RegisterPageTests {
    @BeforeMethod
    public static void setUp(){
        Browser.open();
        HomePage.goTo();
        RegisterPage.goTo();
    }
    @AfterMethod
    public static void tearDown(){
        Browser.close();
    }
    @Test
    public static void registrationTest(){
        CommonVerification.verifyTitle("Register Account");
        RegisterPage.setPersonalDetails("Asen","Ivanov","aaaaaa@abv.bg","1111111","1qaz","1qaz");
        RegisterPage.selectSubscribeOption("yes");
        RegisterPage.privacyPolicyAgreement();
        RegisterPage.submitRegistrationDetails();
        CommonVerification.verifyTitle("Your Account Has Been Created!");
    }
}
